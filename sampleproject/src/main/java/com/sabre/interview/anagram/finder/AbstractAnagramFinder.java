package com.sabre.interview.anagram.finder;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.sabre.interview.anagram.fileloader.AnagramFileLoader;
import com.sabre.interview.anagram.utils.AnagramUtils;

public abstract class AbstractAnagramFinder {

	private Map<String, HashSet<String>> anagramMap = new HashMap<String, HashSet<String>>();

	private AnagramFileLoader anagramFileLoader;
	private AnagramUtils anagramUtils;

	public AbstractAnagramFinder() throws IOException {

	}

	protected AnagramFileLoader getAnagramFileLoader() {
		return anagramFileLoader;
	}

	protected void setAnagramFileLoader(AnagramFileLoader anagramFileLoader) {
		this.anagramFileLoader = anagramFileLoader;
	}

	protected AnagramUtils getAnagramUtils() {
		return anagramUtils;
	}

	protected void setAnagramUtils(AnagramUtils anagramUtils) {
		this.anagramUtils = anagramUtils;
	}

	protected void setAnagrams() throws IOException {
		BufferedReader reader = anagramFileLoader.getAnagramResources();

		while (reader.ready()) {
			String word = anagramUtils.getWordInCorrectFormat(reader);
			String key = anagramUtils.hash(word);
			putAnagramIntoAnagramMap(word, key);
		}
	}

	private void putAnagramIntoAnagramMap(String word, String key) {
		if (!anagramMap.containsKey(key)) {
			anagramMap.put(key, new HashSet<String>());
		}
		anagramMap.get(key).add(word);
	}

	
	public Map<String, HashSet<String>> getAnagramMap() {
		return anagramMap;
	}

	public abstract String getAllAnagrams(); 
	
	public abstract String getLongestAnagram();

	public abstract String getAnagrams(String word);
	
}
