package com.sabre.interview.anagram;

import java.io.IOException;

import com.sabre.interview.anagram.finder.AbstractAnagramFinder;
import com.sabre.interview.anagram.finder.AnagramFinderDefaultResources;

public class AnagramApp {


	public static void main(String[] args) {
		
		try {
			AbstractAnagramFinder anagramFinder = new AnagramFinderDefaultResources();
			System.out.println(anagramFinder.getAllAnagrams());
			System.out.println(anagramFinder.getLongestAnagram());
			System.out.println(anagramFinder.getAnagrams("xxx"));
			System.out.println(anagramFinder.getAnagrams("bela"));
			System.out.println(anagramFinder.getAnagrams("mom"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}




}
