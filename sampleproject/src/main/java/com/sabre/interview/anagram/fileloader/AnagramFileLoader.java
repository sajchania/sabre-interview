package com.sabre.interview.anagram.fileloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class AnagramFileLoader {

	private static String DEFAULT_ANAGRAM_RESOURCES = "wordlist";

	public AnagramFileLoader() {
	}

	public AnagramFileLoader(String path) {
		setPath(path);
	}

	public void setPath(String path) {
		DEFAULT_ANAGRAM_RESOURCES = path;
	}

	public String getPath() {
		return DEFAULT_ANAGRAM_RESOURCES;
	}

	public BufferedReader getAnagramResources() throws FileNotFoundException {
		ClassLoader classLoader = getClass().getClassLoader();
		String pathName = classLoader.getResource(DEFAULT_ANAGRAM_RESOURCES).getFile();
		File file = new File(pathName);

		FileReader fileReader = new FileReader(file);
		BufferedReader reader = new BufferedReader(fileReader);
		return reader;
	}

}
