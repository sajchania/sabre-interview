package com.sabre.interview.anagram.utils;

import java.io.BufferedReader;
import java.io.IOException;

public interface AnagramUtils {
	public String hash(String word);

	public String getWordInCorrectFormat(BufferedReader bufferedReader) throws IOException;
}
