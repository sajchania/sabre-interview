package com.sabre.interview.anagram.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;

public class AnagramUtilsImpl implements AnagramUtils {

	@Override
	public String getWordInCorrectFormat(BufferedReader bufferedReader) throws IOException {
		return bufferedReader.readLine().toLowerCase();
	}

	@Override
	public String hash(String word) {
		char[] chars = word.toCharArray();
		Arrays.sort(chars);
		return new String(chars);
	}

}
