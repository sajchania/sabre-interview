package com.sabre.interview.anagram.finder;

import java.io.IOException;
import java.util.HashSet;

import com.sabre.interview.anagram.fileloader.AnagramFileLoader;
import com.sabre.interview.anagram.utils.AnagramUtilsImpl;

public class AnagramFinderDefaultResources extends AbstractAnagramFinder {

	public static final String ANAGRAMS_WERE_NOT_FOUND = "[no anagrams were found]";
	public static final String WORD_WAS_NOT_FOUND = "[word wasn't found]";
	public static final String WORD_NULL = "[word is equal to null]";
	public static final String ANAGRAM_MAP_NULL = "[anagram map is equal to null]";
	public static final String ANAGRAM_MAP_EMPTY = "[anagram map is empty]";

	public AnagramFinderDefaultResources() throws IOException {
		setAnagramFileLoader(new AnagramFileLoader());
		setAnagramUtils(new AnagramUtilsImpl());
		setAnagrams();
	}

	@Override
	public String getAllAnagrams() {
		StringBuffer result = new StringBuffer();

		if (getAnagramMap() == null) {
			return new String(ANAGRAM_MAP_NULL);
		}
		if (getAnagramMap().isEmpty()) {
			return new String(ANAGRAM_MAP_EMPTY);
		}

		for (HashSet<String> anagramSet : getAnagramMap().values()) {
			if (anagramSet.size() > 1) {
				result.append(anagramSet.toString() + "\n");
			}
		}
		return result.toString();
	}

	@Override
	public String getLongestAnagram() {
		StringBuffer result = new StringBuffer();
		int longestAnagram = 0;

		if (getAnagramMap() == null) {
			return new String(ANAGRAM_MAP_NULL);
		}
		if (getAnagramMap().isEmpty()) {
			return new String(ANAGRAM_MAP_EMPTY);
		}

		for (HashSet<String> anagramSet : getAnagramMap().values()) {
			if (anagramSet.size() > 1) {
				int previousLongestAnagram = longestAnagram;
				longestAnagram = Math.max(anagramSet.size(), longestAnagram);
				if (longestAnagram > previousLongestAnagram) {
					result = new StringBuffer(anagramSet.toString() + "\n");
				} else if (longestAnagram == anagramSet.size()) {
					result.append(anagramSet.toString() + "\n");
				}
			}
		}
		return result.toString();
	}

	@Override
	public String getAnagrams(String word) {
		if (word == null) {
			return new String(WORD_NULL);
		}
		String hashedWord = getAnagramUtils().hash(word);
		if (getAnagramMap() == null) {
			return new String(ANAGRAM_MAP_NULL);
		}
		if (getAnagramMap().isEmpty()) {
			return new String(ANAGRAM_MAP_EMPTY);
		}

		HashSet<String> anagramSet = getAnagramMap().get(hashedWord);
		if (anagramSet == null) {
			return new String(WORD_WAS_NOT_FOUND);
		}
		if (anagramSet.size() < 2) {
			return new String(ANAGRAMS_WERE_NOT_FOUND);
		}

		return anagramSet.toString();
	}

}