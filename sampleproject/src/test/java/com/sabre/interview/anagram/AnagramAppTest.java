package com.sabre.interview.anagram;

import java.io.IOException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sabre.interview.anagram.finder.AbstractAnagramFinder;
import com.sabre.interview.anagram.finder.AnagramFinderDefaultResources;

public class AnagramAppTest {

	static AbstractAnagramFinder anagramFinder;

	@BeforeClass
	public static void initializeDefaultResources() {
		try {
			anagramFinder = new AnagramFinderDefaultResources();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getAnagramsNullValue() {
		String word = null;
		Assert.assertEquals(AnagramFinderDefaultResources.WORD_NULL, anagramFinder.getAnagrams(word));
	}

	@Test
	public void getAnagramsEmptyValue() {
		String word = "";
		Assert.assertEquals(AnagramFinderDefaultResources.WORD_WAS_NOT_FOUND, anagramFinder.getAnagrams(word));
	}

	@Test
	public void getAnagramsSingleValue() {
		String word = "mom";
		Assert.assertEquals(AnagramFinderDefaultResources.ANAGRAMS_WERE_NOT_FOUND, anagramFinder.getAnagrams(word));
	}

	@Test
	public void getAnagramsNotExistingValue() {
		String word = "xxx";
		Assert.assertEquals(AnagramFinderDefaultResources.ANAGRAMS_WERE_NOT_FOUND, anagramFinder.getAnagrams(word));
	}

	@Test
	public void getAnagramsGoodValue() {
		String word = "bela";
		Assert.assertEquals("[blae, albe, abel, bale, bela, able, elba, labe]", anagramFinder.getAnagrams(word));
	}

	@Test
	public void getLongestAnagramGoodValue() {
		Assert.assertEquals(

				"[slater, salter, artels, tarsel, alerts, ratels, alters, estral, staler, stelar, laster, rastle, talers]\n"
						+ "[gantries, gratines, tasering, ganister, tigreans, angriest, ingrates, rangiest, stearing, granites, tangiers, astringe, reasting]\n"
						+ "[teras, treas, taser, aster, tares, tears, resat, rates, stare, reast, strae, arets, stear]\n"

				, anagramFinder.getLongestAnagram());
	}

}
